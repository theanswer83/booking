import {Component} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Hotel} from '../../model/hotel';
import {CartService} from '../../core/services/cart.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: []
})
export class SearchComponent {

  text = 'Milano';
  hotels: Hotel[];
  active: Hotel;
  activeImage: string;

  constructor(private http: HttpClient, public cart: CartService, private router: Router) {
    this.searchHotels(this.text);
  }

  searchHotels(text: string): void {
    this.text = text;
    this.http.get<Hotel[]>('http://localhost:3000/hotels?q=' + text)
      .subscribe(res => {
        if (!res.length){
          this.router.navigateByUrl('search/no-results');
          return;
        }
        this.hotels = res;
        this.setActive(this.hotels[0]);
      });
  }

  setActive(hotel: Hotel): void {
    this.active = hotel;
    this.activeImage = this.active.images[0];
  }

  sendEmail({email, message}): void {
    window.alert(`sent:
      ${email}
      ${message}
      ${this.active?.email}
    `);
  }

}
