import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-rates',
  template: `
    <div class="font-superbig">
      {{rate}}
      <i class="ion-ios-person"></i>
    </div>
  `,
  styles: []
})
export class RatesComponent {
  @Input() rate: number;
}
