import {Component} from '@angular/core';
import {CartService} from '../../core/services/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styles: []
})
export class CartComponent {

  constructor(public cart: CartService) {}

}
