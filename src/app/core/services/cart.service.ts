import {Injectable} from '@angular/core';
import {CartItem} from '../../model/cart-item';
import {Hotel, Room} from '../../model/hotel';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  items: CartItem[] = [];

  addToCart(hotel: Hotel, room: Room): void {
    this.items = [
      ...this.items,
      {
        hotel,
        room,
        creationDate: Date.now()
      }
    ];
  }

  removeFromCart(cartItem: CartItem): void {
    this.items = this.items.filter(item => item.creationDate !== cartItem.creationDate);
  }

  proceed(): void {
    window.alert(this.items.length);
  }
}
