import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-image-gallery',
  template: `
    <img [src]="activeImage" width="100%" height="150">
    <div class="nested">
      <div *ngFor="let image of images" (click)="activeImage = image">
        <img [src]="image" width="100%">
      </div>
    </div>
  `,
  styles: []
})
export class ImageGalleryComponent {
  @Input() images: string[];
  @Input() activeImage: string;
}
